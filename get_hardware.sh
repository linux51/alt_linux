#!/bin/bash

pacman -Sy hwinfo dmidecode lspci lscpu dmesg

hwinfo --short >> hw_short_output
hwinfo >> hw_output
lsblk >> lsblk_output
mount >> mount_output
dmidecode >> dmi_output
lspci >> lspci_output
lscpu >> lscpu_output
dmesg >> dmesg_output
ip link >> iplink_output

