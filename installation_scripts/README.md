## installation_scripts

* dualboot-legacy-only.sh: dualboot in legacy mode, even when USB is booted in UEFI
* dualboot.sh: standard dualboot script
* install-legacy-only.sh: install in legacy mode, even when USB is booted in UEFI
* install_nvme.sh: install on nvme hard drives
* install.sh: standard installation script
* sda10_install.sh: install in legacy mode on sda10 in logical partition
* upd_to_2.0.sh: update older alt linux release to 2.0

