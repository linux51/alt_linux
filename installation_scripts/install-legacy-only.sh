#! /bin/bash
lsblk
echo -----------
fdisk -l | grep "Disk model" -B 1
DISTR_VERSION=$(ls /archive | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
echo "Run installation" $(ls /archive | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')"? yes/no"
read install
if [[ "$install" == "yes" ]]; then
  echo "Select the drive to install. Example sda,sdb,sdc ...."
  read DISK
  echo "install XFCE? yes/no"
  read DE_answer
  if [[ $DE_answer == "yes" ]]; then
    DE=xfce
  elif [ $DE_answer == 'no' ]; then
    DE=plasma
  else
    echo "Incorrect answer"
    exit 1
  fi
  echo "Checking /archive/$(ls /archive | grep alt-system)"
  pv /archive/$(ls /archive | grep alt-system) | tar -I zstdmt -t   > /dev/null
  CHECK_TAR=$?
  if [[ $CHECK_TAR = 0 ]]; then
    if [ -d '/sys/firmware/efi' ] && false
    then
      DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      tempfile=/tmp/hostname_tmp
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_HOSTNAME  --title "Hostname" --clear \
                        --inputbox "Input hostname:" 20 61 2> $tempfile
      retval_hostname=$?
      DIALOG_SOFT=${DIALOG_SOFT=dialog}
      tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_SOFT --backtitle "Alt linux install" \
              --title "Soft installation" --clear \
              --checklist "Choose soft for install" 20 61 5 \
              "msoffice2010" "Install Microsoft office 2010" off \
              "mathematica"  "Mathematica" off \
              "matlab"    "Matlab" off \
              "matlab2012" "matlab 2012" off \
              "salomemeca"    "SalomeMeca" off \
              "siemensnx"    "SiemensNX" off \
              "kompas"    "Kompas v19 v20" off \
              "labview"   "labview install" off \
              "techsoft"    "techsoft" off \
              "quartus"    "quartus" off \
              "mathcad14" "Matcad 2014" off \
              "ansys2019" "Ansys 2019" off \
              "ansys2021" "Ansys 2021" off \
              "xilinx"   "Xilinx" off  2> $tempfile
      retval_soft=$?
      choice=`cat $tempfile`
      parted -a optimal /dev/${DISK} --script mklabel gpt
      parted -a optimal /dev/${DISK} --script mkpart "efi" fat32 1MiB 261MiB
      parted -a optimal /dev/${DISK} --script mkpart "boot" ext2 261MiB 1261MiB
      parted -a optimal /dev/${DISK} --script mkpart "root" ext4 1261MiB 100%
      parted --script /dev/${DISK} set 1 esp on
      parted --script /dev/${DISK} set 1 boot on
      mkfs.fat -F32 /dev/${DISK}1
      mkfs.ext2 -F /dev/${DISK}2
      mkfs.ext4 -F /dev/${DISK}3
      mount /dev/${DISK}3 /mnt
      mkdir /mnt/boot
      mount /dev/${DISK}2 /mnt/boot
      echo "Unpacking /archive/$(ls /archive | grep alt-system)"
      pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt -xp -C /mnt/
      sed -i "/Session/c Session=$DE" /mnt/etc/X11/sddm/sddm.conf 
      for i in $choice; do echo "Unpacking $i" && pv /archive/$i.tar.zst | tar -I zstdmt -xp -C /mnt/ ; done
      HOSTNAME=$(cat /tmp/hostname_tmp)
      echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/etc/hosts
      echo $HOSTNAME > /mnt/etc/hostname
      touch /mnt/etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
      mkdir -p /mnt/proc /mnt/dev/ /mnt/sys /mnt/tmp /mnt/boot/efi /mnt/sys/firmware/efi/efivars
      mount /dev/${DISK}1 /mnt/boot/efi
      for i in /dev /dev/pts /proc /sys /sys/firmware/efi/efivars /run; do mount -B $i /mnt$i; done
      echo -e "#!/bin/bash\n
              /usr/sbin/control su wheelonly
              /usr/sbin/control sudo wheelonly
              grub-install --target=x86_64-efi  --efi-directory=/boot/efi --bootloader-id=altlinux --recheck
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R user:user /home/user
              if [ -d '/opt/soft/advego_plagiatus_3' ]; then
                chmod -R 666 /opt/soft/advego_plagiatus_3/*.log 2>/dev/null
              fi
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              if [[ "$DE_answer" == "no" ]]; then
                echo "removing XFCE"
                apt-get remove --purge xfce4-minimal NetworkManager-applet-gtk -y
              elif [[ "$DE_answer" == "yes" ]]; then
                apt-get install xfce4-xkb-plugin -y
              fi
              " > /mnt/tmp/grub.sh   
      chmod +x /mnt/tmp/grub.sh
      genfstab -U -p /mnt > /mnt/etc/fstab
      chroot /mnt /usr/bin/zsh /tmp/grub.sh
      wget -P /patch -A zst -m -p -E -k -K -npd -e robots=off -R index.html* http://rpl.bmstu.ru/patch/
      for i in $(ls /patch | grep $DISTR_VERSION); do pv /patch/$i | tar -I zstdmt -xp -C /mnt/;done
      umount -l /dev/${DISK}1 /dev/${DISK}2 /dev/${DISK}3
      sh /root/get-info.sh 
    else
      DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      tempfile=/tmp/hostname_tmp
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_HOSTNAME  --title "Hostname" --clear \
                        --inputbox "Input hostname:" 20 61 2> $tempfile
      retval_hostname=$?
      DIALOG_SOFT=${DIALOG_SOFT=dialog}
      tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      trap "rm -f $tempfile" 0 1 2 5 15
      $DIALOG_SOFT --backtitle "Alt linux install" \
              --title "Soft installation" --clear \
              --checklist "Choose soft for install" 20 61 5 \
              "msoffice2010" "Install Microsoft office 2010" off \
              "mathematica"  "Mathematica" off \
              "matlab"    "Matlab" off \
              "matlab2012" "matlab 2012" off \
              "salomemeca"    "SalomeMeca" off \
              "siemensnx"    "SiemensNX" off \
              "kompas"    "Kompas v19 v20" off \
              "labview"   "labview install" off \
              "techsoft"    "techsoft" off \
              "quartus"    "quartus" off \
              "mathcad14" "Matcad 2014" off \
              "ansys2019" "Ansys 2019" off \
              "ansys2021" "Ansys 2021" off \
              "xilinx"   "Xilinx" off  2> $tempfile
      retval_soft=$?
      choice=`cat $tempfile`
      parted -a optimal /dev/${DISK} --script mklabel msdos
      parted -a optimal /dev/${DISK} --script mkpart primary ext4 0% 100%
      parted --script /dev/${DISK} set 1 boot on
      mkfs.ext4 -F /dev/${DISK}1
      mount /dev/${DISK}1 /mnt
      echo "Unarchive /archive/$(ls /archive | grep alt-system)"
      pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt -xp -C /mnt/
      sed -i "/Session/c Session=$DE" /mnt/etc/X11/sddm/sddm.conf 
      for i in $choice; do echo "Unpacking $i" && pv /archive/$i.tar.zst | tar -I zstdmt -xp -C /mnt/ ; done
      mkdir -p /mnt/proc /mnt/dev/ /mnt/sys /mnt/tmp
      HOSTNAME=$(cat /tmp/hostname_tmp)
      echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/etc/hosts
      echo $HOSTNAME > /mnt/etc/hostname
      touch /mnt/etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
      echo -e "#!/bin/bash\n
              /usr/sbin/control su wheelonly
              /usr/sbin/control sudo wheelonly
              grub-install --target=i386-pc /dev/${DISK}
              grub-mkconfig -o /boot/grub/grub.cfg
              chown -R root:root /opt/soft 2>/dev/null
              chmod -R 755 /opt/soft 2>/dev/null
              chown -R user:user /home/user
              if [ -d '/opt/soft/advego_plagiatus_3' ]; then
                chmod -R 666 /opt/soft/advego_plagiatus_3/*.log 2>/dev/null
              fi
              if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                chmod 600 /swapfile
                /sbin/mkswap /swapfile
                /sbin/swapon /swapfile
                echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              fi
              if [[ "$DE_answer" == "no" ]]; then
                echo "Removing XFCE"
                apt-get remove --purge xfce4-minimal NetworkManager-applet-gtk -y
              elif [[ "$DE_answer" == "yes" ]]; then
                apt-get install xfce4-xkb-plugin -y
              fi
              " > /mnt/tmp/grub.sh
      chmod +x /mnt/tmp/grub.sh
      mount -t proc /proc /mnt/proc/
      mount --rbind /sys /mnt/sys/
      mount --rbind /dev /mnt/dev/
      genfstab -U -p /mnt > /mnt/etc/fstab
      chroot /mnt /usr/bin/zsh /tmp/grub.sh
      wget -P /patch -A zst -m -p -E -k -K -npd -e robots=off -R index.html* http://rpl.bmstu.ru/patch/
      for i in $(ls /patch | grep $DISTR_VERSION); do pv /patch/$i | tar -I zstdmt -xp -C /mnt/;done
      umount -l /dev/${DISK}1 
      sh /root/get-info.sh 
    fi
  elif [[ $CHECK_TAR != 0 ]]; then
    echo "/archive/$(ls /archive | grep alt-system) is broken"
    exit 1
  fi
elif [[ "$install" == "no" ]]; then
  exit 1
fi
