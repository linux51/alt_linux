#!/bin/bash
DISK=$2
DE_answer=$1
				mkdir -p /mnt/$DISK/proc /mnt/$DISK/dev/ /mnt/$DISK/sys /mnt/$DISK/tmp
                                HOSTNAME=$(cat /tmp/hostname_tmp)
                                echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/$DISK/etc/hosts
                                echo $HOSTNAME > /mnt/$DISK/etc/hostname
                                touch /mnt/$DISK/etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
                                echo -e "#!/bin/bash\n
                                grub-install /dev/${DISK}
                                grub-mkconfig -o /boot/grub/grub.cfg
                                sed -i '/(on \/dev\//,/}/d' /boot/grub/grub.cfg
                                chown -R root:root /opt/soft 2>/dev/null
                                chmod -R 755 /opt/soft 2>/dev/null
                                chown -R user:user /home/user
				if [ -d '/opt/soft/advego_plagiatus_3' ]; 
				then
                                	chmod -R 666 /opt/soft/advego_plagiatus_3/*.log
				fi
                                if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                                        fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                                        chmod 600 /swapfile
                                        /sbin/mkswap /swapfile
                                        /sbin/swapon /swapfile
                                        echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
                                fi
                                " > /mnt/$DISK/tmp/grub.sh
                                chmod +x /mnt/$DISK/tmp/grub.sh
                                mount -t proc /proc /mnt/$DISK/proc/
                                mount --rbind /sys /mnt/$DISK/sys/
                                mount --rbind /dev /mnt/$DISK/dev/
                                genfstab -U -p /mnt/$DISK > /mnt/$DISK/etc/fstab
                                chroot /mnt/$DISK /usr/bin/zsh /tmp/grub.sh
                                for i in $(ls /patch | grep $DISTR_VERSION); do pv /patch/$i | tar -I zstdmt --numeric-owner -xp -C /mnt/$DISK ;done
                                umount -l /dev/${DISK}1

