#! /bin/bash
#set -x
lsblk
echo -----------
fdisk -l | grep "Disk model" -B 1
echo "Run installation" $(ls /archive | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')"? yes/no"
read install
if [[ "$install" == "yes" ]]; 
then
	KDISKS=`lsblk -o NAME,TYPE,TRAN | grep ' disk ' | grep -v usb | cut -f 1 -d " "`
	DISKNUM=0
	for DISK in `echo $KDISKS`
	do
		DISKNUM=$(($DISKNUM+1))
		D[$DISKNUM]=$DISK
	done
	echo $DISKNUM 
#  	echo "Select the drive to install. Example sda,sdb,sdc ...."
#  	read DISK
  	echo "install XFCE? yes/no"
  	read DE_answer
  	if [[ $DE_answer == "yes" ]]; 
	then
	    DE=xfce
	elif [ $DE_answer == 'no' ]; 
	then
	    DE=plasma
	else
	    echo "Incorrect answer"
	    exit 1
  	fi
  	echo "Checking /archive/$(ls /archive | grep alt-system)"
  	pv /archive/$(ls /archive | grep alt-system) | tar -I zstdmt -t   > /dev/null
  	CHECK_TAR=$?
  	if [[ $CHECK_TAR = 0 ]]; then
    		if [ -d '/sys/firmware/efi' ]
    		then
			DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      			tempfile=/tmp/hostname_tmp
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_HOSTNAME  --title "Hostname" --clear \
                        	--inputbox "Input hostname:" 20 61 2> $tempfile
      			retval_hostname=$?
      			DIALOG_SOFT=${DIALOG_SOFT=dialog}
      			tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_SOFT --backtitle "Alt linux install" \
              		--title "Soft installation" --clear \
              		--checklist "Choose soft for install" 20 61 5 \
              		"msoffice2010" "Install Microsoft office 2010" off \
              		"mathematica"  "Mathematica" off \
              		"ansys2019"  "Ansys" off \
              		"matlab"    "Matlab" off \
              		"salomemeca"    "SalomeMeca" off \
              		"siemensnx"    "SiemensNX" off \
              		"kompas"    "Kompas v19 v20" off \
              		"labview"   "labview install" off \
              		"techsoft"    "techsoft" off \
              		"quartus"    "quartus" off \
              		"xilinx"   "Xilinx" off \  
			"mathcad14" "mathcad14"	2> $tempfile
      			retval_soft=$?
      			choice=`cat $tempfile`
			for DISK in `echo $KDISKS`
			do
      				parted -a optimal /dev/${DISK} --script mklabel gpt
      				parted -a optimal /dev/${DISK} --script mkpart primary fat32 1MiB 261MiB
      				parted -a optimal /dev/${DISK} --script mkpart primary ext2 261MiB 1261MiB
      				parted -a optimal /dev/${DISK} --script mkpart primary ext4 1261MiB 100%
      				parted --script /dev/${DISK} set 1 esp on
      				parted --script /dev/${DISK} set 1 boot on
      				mkfs.fat -F32 /dev/${DISK}1
      				mkfs.ext2 -F /dev/${DISK}2
      				mkfs.ext4 -F /dev/${DISK}3
      				mkdir /mnt/$DISK #
      				mount /dev/${DISK}3 /mnt/$DISK #
      				mkdir /mnt/$DISK/boot #
      				mount /dev/${DISK}2 /mnt/$DISK/boot #
			done
      			echo "Unpacking /archive/$(ls /archive | grep alt-system)"
#      			pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt -xp -C /mnt/
			if [ $DISKNUM -eq 1 ]
			then 
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tar -xp /mnt/${D[1]} #
			fi
			if [ $DISKNUM -eq 2 ]
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tar -xp /mnt/${D[2]} #
			fi
			if [ $DISKNUM -eq 3 ]
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tee >(tar -xp -C /mnt/${D[2]}) | tar -xp /mnt/${D[3]} #
			fi
			if [ $DISKNUM -eq 4 ]
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tee >(tar -xp -C /mnt/${D[2]}) |  tee >(tar -xp -C /mnt/${D[3]}) | tar -xp /mnt/${D[4]} #
			fi
			for DISK in `echo $KDISKS` #
			do
      				sed -i "/Session/c Session=$DE" /mnt/$DISK/etc/X11/sddm/sddm.conf #
			done
#      			for i in $choice; do echo "Unpacking $i" && pv /archive/$i.tar.zst | tar -I zstdmt -xp -C /mnt/ ; done
			for i in $choice; 
			do 
				if [ $DISKNUM -eq 1 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tar -xp -C /mnt/${D[1]} ; 
				fi	
				if [ $DISKNUM -eq 2 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tar -xp -C /mnt/${D[2]} ; 
				fi
				if [ $DISKNUM -eq 3 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tee >(tar -xp -C /mnt/${D[2]}) | tar -xp -C /mnt/${D[3]} ; 
				fi
				if [ $DISKNUM -eq 4 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tee >(tar -xp -C /mnt/${D[2]}) | tee >(tar -xp -C /mnt/${D[3]}) | tar -xp -C /mnt/${D[4]} ; 
				fi

			done 
			for DISK in `echo $KDISKS` #
			do
      				HOSTNAME=$(cat /tmp/hostname_tmp)
      				echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/$DISK/etc/hosts #
      				echo $HOSTNAME > /mnt/$DISK/etc/hostname
      				touch /mnt/$DISK/etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
      				mkdir -p /mnt/$DISK/proc /mnt/$DISK/dev/ /mnt/$DISK/sys /mnt/$DISK/tmp /mnt/$DISK/boot/efi /mnt/$DISK/sys/firmware/efi/efivars
      				mount /dev/${DISK}1 /mnt/$DISK/boot/efi
      				for i in /dev /dev/pts /proc /sys /sys/firmware/efi/efivars /run; do mount -B $i /mnt/${DISK}$i; done
      				echo -e "#!/bin/bash\n
              			/usr/sbin/control su wheelonly
              			/usr/sbin/control sudo wheelonly
              			grub-install --target=x86_64-efi  --efi-directory=/boot/efi --bootloader-id=altlinux --recheck
              			grub-mkconfig -o /boot/grub/grub.cfg
				sed -i '/(on \/dev\//,/}/d' /boot/grub/grub.cfg
              			chown -R root:root /opt/soft 2>/dev/null
              			chmod -R 755 /opt/soft 2>/dev/null
              			chown -R user:user /home/user
              			chmod -R 666 /opt/soft/advego_plagiatus_3/*.log
              			if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                			fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                			chmod 600 /swapfile
                			/sbin/mkswap /swapfile
                			/sbin/swapon /swapfile
                			echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              			fi
              			if [[ $DE_answer == "yes" ]]; then
                			apt-get install NetworkManager-applet-gtk -y
              			elif [ $DE_answer == 'no' ]; then
                			apt-get remove xfce4-minimal -y
              			fi
              			" > /mnt/$DISK/tmp/grub.sh   #
      				chmod +x /mnt/$DISK/tmp/grub.sh #
      				genfstab -U -p /mnt/$DISK > /mnt/$DISK/etc/fstab
      				chroot /mnt/$DISK /usr/bin/zsh /tmp/grub.sh
      				umount -l /dev/${DISK}1 /dev/${DISK}2 /dev/${DISK}3
			done
    		else
      			DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      			tempfile=/tmp/hostname_tmp
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_HOSTNAME  --title "Hostname" --clear \
                	        --inputbox "Input hostname:" 20 61 2> $tempfile
      			retval_hostname=$?
      			DIALOG_SOFT=${DIALOG_SOFT=dialog}
      			tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_SOFT --backtitle "Alt linux install" \
              		--title "Soft installation" --clear \
              		--checklist "Choose soft for install" 20 61 5 \
              		"msoffice2010" "Install Microsoft office 2010" off \
              		"mathematica"  "Mathematica" off \
              		"ansys2019"  "Ansys2019" off \
              		"matlab"    "Matlab" off \
              		"salomemeca"    "SalomeMeca" off \
              		"siemensnx"    "SiemensNX" off \
              		"kompas"    "Kompas v19 v20" off \
              		"labview"   "labview install" off \
              		"techsoft"    "techsoft" off \
              		"quartus"    "quartus" off \
              		"xilinx"   "Xilinx" off \
			"mathcad14" "mathcad14" off 2> $tempfile
      			retval_soft=$?
      			choice=`cat $tempfile`
			for DISK in `echo $KDISKS`
			do
      				parted -a optimal /dev/${DISK} --script mklabel msdos
      				parted -a optimal /dev/${DISK} --script mkpart primary ext4 0% 100%
      				parted --script /dev/${DISK} set 1 boot on
      				mkfs.ext4 -F /dev/${DISK}1
				mkdir /mnt/$DISK
      				mount /dev/${DISK}1 /mnt/$DISK
			done
      			echo "Unarchive /archive/$(ls /archive | grep alt-system)"
#      			pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt -xp -C /mnt/
			if [ $DISKNUM -eq 1 ] 
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tar -xp -C /mnt/${D[2]}
			fi
			if [ $DISKNUM -eq 2 ] 
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tar -xp -C /mnt/${D[2]}
			fi
			if [ $DISKNUM -eq 3 ] 
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tee >(tar -xp -C /mnt/${D[2]}) | tar -xp -C /mnt/${D[3]}
			fi
			if [ $DISKNUM -eq 4 ] 
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tee >(tar -xp -C /mnt/${D[2]}) | tee >(tar -xp -C /mnt/${D[3]}) | tar -xp -C /mnt/${D[4]}
			fi

			for DISK in `echo $KDISKS`
			do
      				sed -i "/Session/c Session=$DE" /mnt/$DISK/etc/X11/sddm/sddm.conf 
			done
			for i in $choice; 
			do 
				if [ $DISKNUM -eq 1 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tar -xp -C /mnt/${D[1]}  ; 
				fi
				if [ $DISKNUM -eq 2 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]}) | tar -xp -C /mnt/${[D2]}  ; 
				fi
				if [ $DISKNUM -eq 3 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]})  | tee >(tar -xp -C /mnt/${D[2]}) | tar -xp -C /mnt/${D[3]}  ; 
				fi
				if [ $DISKNUM -eq 4 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar -xp -C /mnt/${D[1]})  | tee >(tar -xp -C /mnt/${D[2]})  | tee >(tar -xp -C /mnt/${D[3]})  | tar -xp -C /mnt/${D[4]}  ; 
				fi
			done
			for DISK in `echo $KDISKS`
			do
      				mkdir -p /mnt/$DISK/proc /mnt/$DISK/dev/ /mnt/$DISK/sys /mnt/$DISK/tmp
      				HOSTNAME=$(cat /tmp/hostname_tmp)
      				echo -e '127.0.0.1  localhost.localdomain localhost\n127.0.0.1 '$HOSTNAME > /mnt/$DISK/etc/hosts
      				echo $HOSTNAME > /mnt/$DISK/etc/hostname
      				touch /mnt/$DISK/etc/bmstu-$(ls /archive/ | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
      				echo -e "#!/bin/bash\n
              			/usr/sbin/control su wheelonly
              			/usr/sbin/control sudo wheelonly
              			grub-install /dev/${DISK}
              			grub-mkconfig -o /boot/grub/grub.cfg
				sed -i '/(on \/dev\//,/}/d' /boot/grub/grub.cfg
              			chown -R root:root /opt/soft 2>/dev/null
              			chmod -R 755 /opt/soft 2>/dev/null
              			chown -R user:user /home/user
              			chmod -R 666 /opt/soft/advego_plagiatus_3/*.log
              			if [[ $(free -m | awk 'NR == 2{print $2}') -lt 5000 ]]; then
                			fallocate -l $(free -m | awk 'NR == 2{print $2}')M /swapfile
                			chmod 600 /swapfile
                			/sbin/mkswap /swapfile
                			/sbin/swapon /swapfile
                			echo '/swapfile swap swap defaults 0 0' >> /etc/fstab
              			fi
              			if [[ $DE_answer == "yes" ]]; then
                			apt-get install NetworkManager-applet-gtk -y
              			elif [ $DE_answer == 'no' ]; then
                			apt-get remove --purge xfce4-minimal -y
              			fi
              			" > /mnt/$DISK/tmp/grub.sh
      				chmod +x /mnt/$DISK/tmp/grub.sh
      				mount -t proc /proc /mnt/$DISK/proc/
      				mount --rbind /sys /mnt/$DISK/sys/
      				mount --rbind /dev /mnt/$DISK/dev/
      				genfstab -U -p /mnt/$DISK > /mnt/$DISK/etc/fstab
      				chroot /mnt/$DISK /usr/bin/zsh /tmp/grub.sh
      				umount -l /dev/${DISK}1 
			done
    		fi
	elif [[ $CHECK_TAR != 0 ]]; then
    		echo "/archive/$(ls /archive | grep alt-system) is broken"
    		exit 1
  	fi
elif [[ "$install" == "no" ]]; then
	exit 1
fi

