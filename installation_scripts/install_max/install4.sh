#! /bin/bash
#set -x

lsblk
echo ----------- version 0.0.6
timedatectl set-ntp yes
fdisk -l | grep "Disk model" -B 1
DISTR_VERSION=$(ls /archive | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')
echo "Run installation" $(ls /archive | grep alt-system | grep -Eo '[0-9].[0-9].[0-9]{1,4}')"? === All DISK will be destroyed ) === yes/no"
read install
if [[ "$install" == "yes" ]]; 
then
	KDISKS=`lsblk -o NAME,TYPE,TRAN | grep ' disk ' | grep -v usb | cut -f 1 -d " "`
	DISKNUM=0
	for DISK in `echo $KDISKS`
	do
		DISKNUM=$(($DISKNUM+1))
		D[$DISKNUM]=$DISK
	done
	echo $DISKNUM 
#  	echo "Select the drive to install. Example sda,sdb,sdc ...."
#  	read DISK
  	echo "install XFCE? yes/no"
  	read DE_answer
  	if [[ $DE_answer == "yes" ]]; 
	then
	    DE=xfce
	elif [ $DE_answer == 'no' ]; 
	then
	    DE=plasma
	else
	    echo "Incorrect answer"
	    exit 1
  	fi
  	echo "Checking /archive/$(ls /archive | grep alt-system)"
#  	pv /archive/$(ls /archive | grep alt-system) | tar -I zstdmt -t   > /dev/null
#  	CHECK_TAR=$?
	CHECK_TAR=0
  	if [[ $CHECK_TAR = 0 ]]; then
    		if [ -d '/sys/firmware/efi' ]
    		then
			DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      			tempfile=/tmp/hostname_tmp
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_HOSTNAME  --title "Hostname" --clear \
                        	--inputbox "Input hostname:" 20 61 2> $tempfile
      			retval_hostname=$?
      			DIALOG_SOFT=${DIALOG_SOFT=dialog}
      			tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_SOFT --backtitle "Alt linux install" \
              		--title "Soft installation" --clear \
              		--checklist "Choose soft for install" 20 61 5 \
              		"msoffice2010" "Install Microsoft office 2010" off \
              		"mathematica"  "Mathematica" off \
              		"matlab"    "Matlab" off \
			"matlab2012" "matlab 2012" off \
              		"salomemeca"    "SalomeMeca" off \
              		"siemensnx"    "SiemensNX" off \
              		"kompas"    "Kompas v19 v20" off \
              		"labview"   "labview install" off \
              		"techsoft"    "techsoft" off \
              		"quartus"    "quartus" off \
			"mathcad14" "Matcad 2014" off \
	                "ansys2021" "Ansys 2021" off \
        	        "stm32cubdeide" "STM32CubdeIDE" off \
              		"xilinx"   "Xilinx" off 2> $tempfile
      			retval_soft=$?
      			choice=`cat $tempfile`
			for DISK in `echo $KDISKS`
			do
      				parted -a optimal /dev/${DISK} --script mklabel gpt
      				parted -a optimal /dev/${DISK} --script mkpart primary "efi" fat32 1MiB 261MiB
      				parted -a optimal /dev/${DISK} --script mkpart primary "boot" ext2 261MiB 1261MiB
      				parted -a optimal /dev/${DISK} --script mkpart primary "root" ext4 1261MiB 100%
      				parted --script /dev/${DISK} set 1 esp on
      				parted --script /dev/${DISK} set 1 boot on
      				mkfs.fat -F32 /dev/${DISK}1
      				mkfs.ext2 -F /dev/${DISK}2
      				mkfs.ext4 -F /dev/${DISK}3
a      				mkdir /mnt/$DISK #
      				mount /dev/${DISK}3 /mnt/$DISK #
      				mkdir /mnt/$DISK/boot #
      				mount /dev/${DISK}2 /mnt/$DISK/boot #
			done
      			echo "Unpacking /archive/$(ls /archive | grep alt-system)"
#      			pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt --numeric-owner -xp -C /mnt/
			if [ $DISKNUM -eq 1 ]
			then 
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tar --numeric-owner -xp /mnt/${D[1]} #
			fi
			if [ $DISKNUM -eq 2 ]
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tar --numeric-owner -xp /mnt/${D[2]} #
			fi
			if [ $DISKNUM -eq 3 ]
			then
				echo "AAAAAAAAAAAAA"
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[2]}) | tar --numeric-owner -xp /mnt/${D[3]} #
			fi
			if [ $DISKNUM -eq 4 ]
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[2]}) |  tee >(tar --numeric-owner -xp -C /mnt/${D[3]}) | tar --numeric-owner -xp /mnt/${D[4]} #
			fi
			for DISK in `echo $KDISKS` #
			do
      				sed -i "/Session/c Session=$DE" /mnt/$DISK/etc/X11/sddm/sddm.conf #
				sed -i "s/GRUB_GFXMODE='800x600'/GRUB_GFXMODE='text'/" /mnt/$DISK/etc/sysconfig/grub2
			        sed -i "s/GRUB_TERMINAL_OUTPUT='gfxterm'/GRUB_TERMINAL_OUTPUT='console'/" /mnt/$DISK/etc/sysconfig/grub2
			done
#      			for i in $choice; do echo "Unpacking $i" && pv /archive/$i.tar.zst | tar -I zstdmt -xp -C /mnt/ ; done
			for i in $choice; 
			do 
				if [ $DISKNUM -eq 1 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tar --numeric-owner -xp -C /mnt/${D[1]} ; 
				fi	
				if [ $DISKNUM -eq 2 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tar --numeric-owner -xp -C /mnt/${D[2]} ; 
				fi
				if [ $DISKNUM -eq 3 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[2]}) | tar --numeric-owner -xp -C /mnt/${D[3]} ; 
				fi
				if [ $DISKNUM -eq 4 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[2]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[3]}) | tar --numeric-owner -xp -C /mnt/${D[4]} ; 
				fi

			done 
			echo $KDISKS | xargs -n 1 -P 4 ./install4-part-efi.sh $DE_answer
    		else
      			DIALOG_HOSTNAME=${DIALOG_HOSTNAME=dialog}
      			tempfile=/tmp/hostname_tmp
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_HOSTNAME  --title "Hostname" --clear \
                	        --inputbox "Input hostname:" 20 61 2> $tempfile
      			retval_hostname=$?
      			DIALOG_SOFT=${DIALOG_SOFT=dialog}
      			tempfile=`mktemp 2>/dev/null` || tempfile=/tmp/test$$
      			trap "rm -f $tempfile" 0 1 2 5 15
      			$DIALOG_SOFT --backtitle "Alt linux install" \
              		--title "Soft installation" --clear \
              		--checklist "Choose soft for install" 20 61 5 \
              		"msoffice2010" "Install Microsoft office 2010" off \
              		"mathematica"  "Mathematica" off \
              		"matlab"    "Matlab" off \
			"matlab2012" "matlab 2012" off \
              		"salomemeca"    "SalomeMeca" off \
              		"siemensnx"    "SiemensNX" off \
              		"kompas"    "Kompas v19 v20" off \
              		"labview"   "labview install" off \
              		"techsoft"    "techsoft" off \
              		"quartus"    "quartus" off \
			"mathcad14" "Matcad 2014" off \
	     	        "ansys2021" "Ansys 2021" off \
          		"stm32cubdeide" "STM32CubdeIDE" off \
              		"xilinx"   "Xilinx" off  2> $tempfile
      			retval_soft=$?
      			choice=`cat $tempfile`
			for DISK in `echo $KDISKS`
			do
      				parted -a optimal /dev/${DISK} --script mklabel msdos
      				parted -a optimal /dev/${DISK} --script mkpart primary ext4 0% 100%
      				parted --script /dev/${DISK} set 1 boot on
      				mkfs.ext4 -F /dev/${DISK}1
				mkdir /mnt/$DISK
      				mount /dev/${DISK}1 /mnt/$DISK
			done
      			echo "Unarchive /archive/$(ls /archive | grep alt-system)"
#      			pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | tar -I zstdmt -xp -C /mnt/
			if [ $DISKNUM -eq 1 ] 
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tar --numeric-owner -xp -C /mnt/${D[2]}
			fi
			if [ $DISKNUM -eq 2 ] 
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tar --numeric-owner -xp -C /mnt/${D[2]}
			fi
			if [ $DISKNUM -eq 3 ] 
			then
				echo "AAAAAAAAA"
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[2]}) | tar --numeric-owner -xp -C /mnt/${D[3]}
			fi
			if [ $DISKNUM -eq 4 ] 
			then
				pv /archive/$(ls /archive/ | grep system | sort | tail -n 1) | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[2]}) | tee >(tar --numeric-owner -xp -C /mnt/${D[3]}) | tar --numeric-owner -xp -C /mnt/${D[4]}
			fi

			for DISK in `echo $KDISKS`
			do
      				sed -i "/Session/c Session=$DE" /mnt/$DISK/etc/X11/sddm/sddm.conf 
				sed -i "s/GRUB_GFXMODE='800x600'/GRUB_GFXMODE='text'/" /mnt/$DISK/etc/sysconfig/grub2
      sed -i "s/GRUB_TERMINAL_OUTPUT='gfxterm'/GRUB_TERMINAL_OUTPUT='console'/" /mnt/$DISK/etc/sysconfig/grub2

			done
			for i in $choice; 
			do 
				if [ $DISKNUM -eq 1 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tar --numeric-owner -xp -C /mnt/${D[1]}  ; 
				fi
				if [ $DISKNUM -eq 2 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]}) | tar --numeric-owner -xp -C /mnt/${[D2]}  ; 
				fi
				if [ $DISKNUM -eq 3 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]})  | tee >(tar --numeric-owner -xp -C /mnt/${D[2]}) | tar --numeric-owner -xp -C /mnt/${D[3]}  ; 
				fi
				if [ $DISKNUM -eq 4 ]
				then
					echo "Unpacking $i" && pv /archive/$i.tar.zst | zstdmt -c -d | tee >(tar --numeric-owner -xp -C /mnt/${D[1]})  | tee >(tar --numeric-owner -xp -C /mnt/${D[2]})  | tee >(tar --numeric-owner -xp -C /mnt/${D[3]})  | tar --numeric-owner -xp -C /mnt/${D[4]}  ; 
				fi
			done
			echo $KDISKS | xargs -n 1 -P 4 ./install4-part-legacy.sh $DE_answer
    		fi
	elif [[ $CHECK_TAR != 0 ]]; then
    		echo "/archive/$(ls /archive | grep alt-system) is broken"
    		exit 1
  	fi
elif [[ "$install" == "no" ]]; then
	exit 1
fi

