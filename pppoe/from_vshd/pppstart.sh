#!/bin/bash

interface=$( echo $(/sbin/route -n | grep -m 1 "^0\.0\.0\.0" | awk '{print $8}'))

 poff dsl-provider >/dev/null

echo -n "Login: "
read L

echo -n "Password: "
read -s P

LP=`echo "$L * $P"`

 cp /usr/share/scripts/dsl-provider /etc/ppp/peers/
 sed -i "s/plugin rp-pppoe.so/plugin rp-pppoe.so $interface/" /etc/ppp/peers/dsl-provider
 echo "$LP" > /etc/ppp/pap-secrets
 echo "$LP" > /etc/ppp/chap-secrets
 echo 'user "'$L'"' >>/etc/ppp/peers/dsl-provider

 pon dsl-provider

sleep 3

 cp /dev/null /etc/ppp/chap-secrets
 cp /dev/null /etc/ppp/pap-secrets
