#!/bin/bash

export http_proxy=http://openproxy.bmstu.ru:3128/

# apt-get update
# apt-get install rp-pppoe-gui

# pppoe-setup
sed -i -e s,'#USERNAME',"USER=$(hostname)", pppoe.conf
cp -v pppoe.conf /etc/ppp

echo "Install PPPOE for single user, or many?: (s/m)"
read mode

if [[ $mode == [Mm] ]]; then
        sed -i '/#loginfo/a read -s P' pppoestart
        sed -i '/#loginfo/a echo -n "Password: "' pppoestart
        sed -i '/#loginfo/a read L' pppoestart
        sed -i '/#loginfo/a echo -n "Login: "' pppoestart
elif [[ $mode == [Ss] ]]; then
	echo "Input user login: "
	read ulog
	echo "Input user password: "
	read upas
	sed -i "/#loginfo/a L=$ulog" pppoestart
        sed -i "/#loginfo/a P=$upas" pppoestart
else
        echo "There are only 2 options"
        exit 1
fi

cp -v pppoestart /usr/bin
cp -v pppoestop /usr/bin

# gpasswd -d user wheel
# chmod +x /usr/bin/sudo
# chmod +x /bin/su

cp -v sus/pppoe /etc/sudoers.d
# cp -v sus/su /etc/sudoers.d

cp -v icon/start.desktop /home/user/Рабочий\ стол
cp -v icon/stop.desktop /home/user/Рабочий\ стол

cp -v icon/.start_script.sh /home/user/Документы
cp -v icon/.stop_script.sh /home/user/Документы

