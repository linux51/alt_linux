#!/bin/bash

echo "Имя локальной учетной записи: "
read uname

echo "Имя сетевого ресурса: "
read fname

mkdir /home/$uname/$fname
chown -R $uname:$uname /home/$uname/$fname

chmod +srwx /bin/mount 
chmod +srwx /bin/umount 
chmod +srwx /sbin/mount.cifs

echo "Имя пользователя для подключения сетевого ресурса: "
read disk_username
echo "Пароль для подключения сетового ресурса: "
read disk_password

touch /home/$uname/.smbc
echo "username=$disk_username" >> /home/$uname/.smbc
echo "password=$disk_password" >> /home/$uname/.smbc

echo "//8.8.8.8/$fname /home/$uname/$fname cifs rw,credentials=/home/$uname/.smbc,user,_netdev 0 0" >> /etc/fstab
mount -a

