#!/bin/bash
echo " ubuntu or astra or lite?"
read HOST

case $HOST in
ubuntu|Ubuntu|UBUNTU)

	apt-get -y update
	apt-get -y install fusioninventory-agent
	rm -rf /etc/fusioninventory/agent.cfg
	cp /git/Bash_Scripts/fusion_agent/agent.cfg /etc/fusioninventory/agent.cfg
	systemctl restart fusioninventory-agent.service && systemctl enable fusioninventory-agent.service
	systemctl status fusioninventory-agent.service
	;;

astra|Astra|ASTRA)

	# add debian repoitory
	apt-get install -y debian-archive-keyring dirmngr
	echo "deb https://mirror.yandex.ru/debian stretch main contrib non-free" >> /etc/apt/sources.list
	apt-get -y update

	# download dependencies
	apt -y install dmidecode hwdata ucf hdparm
	apt -y install perl libuniversal-require-perl libwww-perl libparse-edid-perl
	apt -y install libproc-daemon-perl libfile-which-perl libhttp-daemon-perl
	apt -y install libxml-treepp-perl libyaml-perl libnet-cups-perl libnet-ip-perl
	apt -y install libdigest-sha-perl libsocket-getaddrinfo-perl libtext-template-perl
	apt -y install libxml-xpath-perl libyaml-tiny-perl

	# download and install fusion agent
	cd / && wget https://github.com/fusioninventory/fusioninventory-agent/releases/download/2.6/fusioninventory-agent_2.6-1_all.deb
	dpkg -i fusioninventory-agent_2.6-1_all.deb
	apt-get -y update

	# copy configuration file and restart service
	rm -rf /etc/fusioninventory/agent.cfg
	cp /git/Bash_Scripts/fusion_agent/agent.cfg /etc/fusioninventory/agent.cfg
	systemctl restart fusioninventory-agent.service && systemctl enable fusioninventory-agent.service
	sed -e '/stretch/ s/^#*/#/' -i /etc/apt/sources.list
	apt-get -y update 
	systemctl status fusioninventory-agent.service
	;;
	
lite|Lite|LITE)

	rm -rf /etc/fusioninventory/agent.cfg
	cp /git/Bash_Scripts/fusion_agent/agent.cfg /etc/fusioninventory/agent.cfg
        systemctl restart fusioninventory-agent.service && systemctl enable fusioninventory-agent.service
        systemctl status fusioninventory-agent.service
	;;
	esac

