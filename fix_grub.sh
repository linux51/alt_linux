#! /bin/bash 
 
diskw=$(lsblk -f|grep ntfs|cut -c 3-|cut -c -3|awk 'NR==1{print $1}')
mount /dev/${diskw}1 /mnt/
sed -i "s/GRUB_GFXMODE='800x600'/GRUB_GFXMODE='text'/" /mnt/etc/sysconfig/grub2 
sed -i 's/CLASS \-\-class os \\/CLASS \-\-class os \-\-unrestricted \\/' /mnt/etc/grub.d/30_os-prober 
os-prober 
grub-mkconfig -o /boot/grub/grub.cfg

