# alt_linux

Scripts to help configure Alt Linux Education 9.2

**Directories**
* installation_scripts: different Alt Linux installation scripts
* pantum_driver: install pantum-m6500-series driver
* pppoe: establish pppoe connection
* wallpapers: BMSTU wallpapers

**Scripts**
* add_netdisk.sh: add network disks shared from windows servers
* fix_grub.sh: change grub resolution and remove password
* get_hardware.sh: get hardware information for full troubleshooting
* get-info.sh: get hardware information after successful installation
* rdesktop_install.sh: RDP connection to windows server 2008
* vladsultant.sh: install and automount consultant plus
* wireshark_update.sh: wireshark installation script
