#!/bin/bash
apt-get update
apt-get install -y wireshark
groupadd wireshark
usermod -a -G wireshark user
chgrp wireshark /usr/bin/dumpcap
chmod 777 /usr/bin/dumpcap
setcap 'CAP_NET_RAW+eip CAP_NET_ADMIN+eip' /usr/bin/dumpcap
getcap /usr/bin/dumpcap

