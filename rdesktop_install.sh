#!/bin/bash

cd /home/user
git clone https://github.com/rdesktop/rdesktop.git
cd /home/user/rdesktop
apt-get install -y autoconf automake
./bootstrap
apt-get install -y libXcursor-devel libtasn1-devel nettle* gnutls* pcsc-*
./configure --disable-credssp
make
make install

echo '8.8.8.8 domain-name' >> /etc/hosts

touch /home/user/run_rdp.sh
echo 'rdesktop -u username PHOTONICS-ZEMAX' >> /home/user/run_rdp.sh

touch /home/user/.local/share/applications/rdp_connect.desktop
cat << EOF > /home/user/.local/share/applications/rdp_connect.desktop
#!/usr/bin/env xdg-open
[Desktop Entry]
Name=rdp_connect
Comment=RDP connection to zemax server
Exec=sh /home/user/run_rdp.sh
Terminal=true
Type=Application
StartupNotify=true
EOF

cp /home/user/.local/share/applications/rdp_connect.desktop /home/user/Рабочий\ стол/

echo '[SCRIPT COPMLETE]'

